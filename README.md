# Mon Application Flask

Ceci est une application Flask de base pour démontrer comment utiliser Flask avec Docker et Docker Compose.

## Prérequis

Vous devez avoir Docker, Docker Compose et Git installés sur votre machine. 

Vous pouvez télécharger Docker à partir de [ici](https://www.docker.com/products/docker-desktop) et Docker Compose à partir de [ici](https://docs.docker.com/compose/install/).

## Comment démarrer

1. Clonez le dépôt :

    ```
    git clone https://gitlab.com/spham/flask_docker.git
    ```

2. Allez dans le répertoire du projet :

    ```
    cd mon-application-flask
    ```

3. Lancez l'application avec Docker Compose :

    ```
    docker-compose up
    ```

    Cette commande démarre le conteneur et mappe le port 5000 du conteneur sur le port 5000 de votre machine. Elle crée également un volume Docker qui mappe le répertoire courant sur votre machine (`$(pwd)`) sur le répertoire `/app` dans le conteneur, vous permettant de modifier le code en temps réel.

## Accéder à l'application

Une fois que l'application est en cours d'exécution, vous pouvez y accéder en ouvrant votre navigateur web et en allant à l'adresse suivante :

http://localhost:5000
