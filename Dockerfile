# On utilise une image de base Python
FROM python:3.9-slim-buster

# Mettre en place le répertoire de travail
WORKDIR /app

# Installer les dépendances de l'application
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

# Copier le reste du code de l'application
COPY . .

# Exposer le port sur lequel l'application sera exécutée
EXPOSE 5000

# Définir la variable d'environnement pour indiquer à flask où trouver l'application
ENV FLASK_APP=app.py
ENV FLASK_ENV=development
ENV FLASK_RUN_HOST=0.0.0.0

# Démarrer l'application
CMD ["flask", "run"]
